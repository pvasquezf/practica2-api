# Practica2-API

Servicios para practica 2 - Analisis y diseño de sistemas

## Apis

** https://practica2-api.azurewebsites.net/api/usuario POST
Formato JSON
```
{
    "Id_usuario":0,
    "User_name":"ANDREE",
    "Correo":"aavalosoto@gmail.com",
    "Pass":"1234"
}
```
** https://practica2-api.azurewebsites.net/api/login POST
Regresa el id_usuario si son las credenciales correctas o -1 si no lo encuentra
```
{
	"User_name":"AndreeAvalos",
	"Password":"1234"
}
```
** https://practica2-api.azurewebsites.net/api/cuenta/id_usuario GET  
donde id_usuario es el id devuelto en la api de arriba
```
{
	"Id_cuenta":1,
	"Fecha_creacion":"somefecha",
	"Estado":1,
	"Id_usuario":1
}
```
** https://practica2-api.azurewebsites.net/api/cuenta POST 
donde id_cuenta y fecha_creacion no importa que se manda, ya que se genera automaticamente
```
{
	"Id_cuenta":-1,
	"Fecha_creacion":"",
	"Estado":1,
	"Id_usuario":1
}
```
** https://practica2-api.azurewebsites.net/api/producto
Obtiene todos los productos de el sistemas
```
[
    {
        "Id_producto": 1,
        "Nombre": "PEPSI",
        "Descripcion": "Agua gaseosa 3L",
        "Precio_base": 15.0
    },
    {
        "Id_producto": 2,
        "Nombre": "COCACOLA",
        "Descripcion": "Agua gaseosa 2.5L",
        "Precio_base": 18.0
    },
    {
        "Id_producto": 3,
        "Nombre": "DELFINES",
        "Descripcion": "Gomitas estilo delfin",
        "Precio_base": 6.0
    },
    {
        "Id_producto": 4,
        "Nombre": "PEPSI",
        "Descripcion": "Agua gaseosa 600ml",
        "Precio_base": 6.0
    },
    {
        "Id_producto": 5,
        "Nombre": "SWITCH",
        "Descripcion": "Video Consola de Nintendo",
        "Precio_base": 4000.0
    },
    {
        "Id_producto": 6,
        "Nombre": "PLAYSTATION 4",
        "Descripcion": "Video Consola de Sony",
        "Precio_base": 3500.0
    },
    {
        "Id_producto": 7,
        "Nombre": "XBOX ONE",
        "Descripcion": "Video Consola de Microsoft",
        "Precio_base": 3800.0
    }
]
```
** https://practica2-api.azurewebsites.net/api/cotizacion/id_cuenta GET
donde id_cuenta es el numero de cuenta del cliente, obtiene todas las cotizaciones del cliente
```
[
    {
        "Id_cotizacion": 1,
        "Id_cuenta": 2,
        "Fecha_cotizacion": "14/09/2019 04:54:32 AM",
        "Precio_total": 0.0
    },
    {
        "Id_cotizacion": 2,
        "Id_cuenta": 2,
        "Fecha_cotizacion": "14/09/2019 05:04:40 AM",
        "Precio_total": 50.0
    }
]
```
** https://practica2-api.azurewebsites.net/api/cotizacion POST
```
    {
        "Id_cotizacion": -1,
        "Id_cuenta": 2,
        "Fecha_cotizacion": "",
        "Precio_total": 0.0
    }
```
** https://practica2-api.azurewebsites.net/api/cotizacion/id_cotizacion PUT
donde id_cotizacion es la cotizacion actual
Actualizar el precio total de una cotizacion, solo importa el precio en el cuerpo del json 
```
    {
        "Id_cotizacion":-1,
        "Id_cuenta": -1,
        "Fecha_cotizacion": "",
        "Precio_total": 50.0
    }
```
** https://practica2-api.azurewebsites.net/api/cotizacionproducto/id_cotizacion GET
donde id_cotizacion es el id de la cotizacion deseada
Trae todos los productos cotizados en la cotizacion
```
[
    {
        "Id_CP": 2,
        "Id_cotizacion": 1,
        "Id_producto": 1,
        "Cantidad": 5,
        "Subtotal": 75.0
    }
]
```
** https://practica2-api.azurewebsites.net/api/cotizacionproducto/id_CP DELETE
donde id_cp es el id del producto de la cotizacion que quiere eliminar
devuelve true o false si logro o no eliminarlo

** https://practica2-api.azurewebsites.net/api/cotizacionproducto POST
devuelve true o false si logro o no eliminarlo
```
    {
        "Id_CP": -1,
        "Id_cotizacion": 1,
        "Id_producto": 1,
        "Cantidad": 8,
        "Subtotal": 0.0
    }

```
