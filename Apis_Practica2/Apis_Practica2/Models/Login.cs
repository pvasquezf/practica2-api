﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apis_Practica2.Models
{
    public class Login
    {
        public string User_name { get; set; }
        public string Password { get; set; }
    }
}
