﻿using System.Collections.Generic;

namespace Apis_Practica2.Models
{
    public class Generico2
    {
        private List<Generico> lst;

        public Generico2()
        {
            lst = new List<Generico>();
        }

        public List<Generico> Lst { get => lst; set => lst = value; }
    }
}
