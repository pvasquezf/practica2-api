﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apis_Practica2.Models
{
    public class Producto
    {
        int id_producto;
        string nombre, descripcion;
        double precio_base;

        public Producto(int id_producto, string nombre, string descripcion, double precio_base)
        {
            this.id_producto = id_producto;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.precio_base = precio_base;
        }

        public int Id_producto { get => id_producto; set => id_producto = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public double Precio_base { get => precio_base; set => precio_base = value; }
    }
}
