﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apis_Practica2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Apis_Practica2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CuentaController : ControllerBase
    {

        // GET: api/Cuenta/5
        [HttpGet("{id}", Name = "Get")]
        public int Get(int id)
        {
            try
            {
                string query = "select * from cuenta where id_usuario="+id;
                Conexion conn = new Conexion();
                List<Generico2> lst1 = conn.metodo_consulta(query, 4);
                foreach (Generico2 item in lst1)
                {
                    Cuenta new_cuenta = new Cuenta(
                        Convert.ToInt32(item.Lst[0].Parametro.ToString()),
                        item.Lst[1].Parametro.ToString(),
                        Convert.ToInt32(item.Lst[2].Parametro.ToString()),
                        Convert.ToInt32(item.Lst[3].Parametro.ToString())
                        );
                    return new_cuenta.Id_cuenta;
                }
                return -1;
            }
            catch (Exception)
            {

                return -1;
            }
        }

        // POST: api/Cuenta
        [HttpPost]
        public int Post(Cuenta entrada)
        {
            try
            {
                Conexion conn = new Conexion();
                List<Generico> lst = new List<Generico>
                {
                    new Generico("estado",entrada.Estado,1),
                    new Generico("id_usuario",entrada.Id_usuario,1)
                };
                return conn.Post("insertCuenta", lst);
            }
            catch (Exception)
            {
                return -1;
            }
        }


    }
}
