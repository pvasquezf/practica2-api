﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apis_Practica2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Apis_Practica2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CotizacionProductoController : ControllerBase
    {

        // GET: api/CotizacionProducto/5
        [HttpGet("{id}", Name = "Get3")]
        public string Get(int id)
        {
            try
            {
                string query = "select * from cotizacion_producto where id_cotizacion =" + id;
                Conexion conn = new Conexion();
                List<Generico2> lst1 = conn.metodo_consulta(query, 5);
                List<Cotizacion_Producto> cotizacions = new List<Cotizacion_Producto>();
                for (int i = 0; i < lst1.Count; i++)
                {
                    Cotizacion_Producto nuevo = new Cotizacion_Producto(
                        Convert.ToInt32(lst1[i].Lst[0].Parametro.ToString()),
                        Convert.ToInt32(lst1[i].Lst[1].Parametro.ToString()),
                        Convert.ToInt32(lst1[i].Lst[2].Parametro.ToString()),
                        Convert.ToInt32(lst1[i].Lst[3].Parametro.ToString()),
                        Convert.ToDouble(lst1[i].Lst[4].Parametro.ToString())
                        );
                    cotizacions.Add(nuevo);
                }
                return JsonConvert.SerializeObject(cotizacions);
            }
            catch { return "NO EXISTEN DATOS"; }
        }

        // POST: api/CotizacionProducto
        [HttpPost]
        public bool Post(Cotizacion_Producto entrada)
        {
            try
            {
                Conexion conn = new Conexion();
                List<Generico> lst = new List<Generico>
                {
                    new Generico("id_cotizacion",entrada.Id_cotizacion,1),
                    new Generico("id_producto",entrada.Id_producto,1),
                    new Generico("cantidad",entrada.Cantidad,1)
                };
                return conn.metodo_proc("insertCotizacion_Producto", lst);
            }
            catch (Exception)
            {
                return false;
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            try
            {
                Conexion conn = new Conexion();
                List<Generico> lst = new List<Generico>
                {
                    new Generico("id_CP",id,1)
                };
                return conn.metodo_proc("deleteCotizacionProducto", lst);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
