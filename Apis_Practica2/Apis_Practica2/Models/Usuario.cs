﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apis_Practica2.Models
{
    public class Usuario
    {
        public int Id_usuario { get; set; }
        public string User_name { get; set; }
        public string Correo { get; set; }
        public string Pass { get; set; }
    }
}
