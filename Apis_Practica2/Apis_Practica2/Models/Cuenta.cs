﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apis_Practica2.Models
{
    public class Cuenta
    {
        int id_cuenta;
        string fecha_creacion;
        int estado, id_usuario;

        public Cuenta(int id_cuenta, string fecha_creacion, int estado, int id_usuario)
        {
            this.id_cuenta = id_cuenta;
            this.fecha_creacion = fecha_creacion;
            this.estado = estado;
            this.id_usuario = id_usuario;
        }

        public int Id_cuenta { get => id_cuenta; set => id_cuenta = value; }
        public string Fecha_creacion { get => fecha_creacion; set => fecha_creacion = value; }
        public int Estado { get => estado; set => estado = value; }
        public int Id_usuario { get => id_usuario; set => id_usuario = value; }
    }
}
