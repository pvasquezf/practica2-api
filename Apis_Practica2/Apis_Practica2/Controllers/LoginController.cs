﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Apis_Practica2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Apis_Practica2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly string cadenaconexion = "Server=tcp:analisis1-1s2019.database.windows.net,1433;Initial Catalog=Practica2;Persist Security Info=False;User ID=admin1;Password=1234admin!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";


        // POST: api/Login
        [HttpPost]
        public int Post(Login usuario)
        {
            try
            {
                SqlDataReader reader = null;
                SqlConnection conexion = new SqlConnection
                {
                    ConnectionString = cadenaconexion
                };
                string query = "Select pass, id_usuario from usuario where user_name=\'"+usuario.User_name+"\';";


                SqlCommand cmd = new SqlCommand
                {
                    CommandType = CommandType.Text,
                    CommandText = query,
                    Connection = conexion
                };

                conexion.Open();
                reader = cmd.ExecuteReader();
                string passAux = "";
                int id_usuario = -1;
                try
                {
                    while (reader.Read())
                    {
                        passAux = reader.GetValue(0).ToString();
                        id_usuario = Convert.ToInt32(reader.GetValue(1).ToString());
                    }

                    if (passAux.Equals(usuario.Password))
                    {
                        conexion.Close();
                        return id_usuario;
                    }

                }
                catch (Exception)
                {
                    conexion.Close();
                    return -1;
                }
                conexion.Close();
                return -1;
            }
            catch { return -1; }

        }
    }
}
