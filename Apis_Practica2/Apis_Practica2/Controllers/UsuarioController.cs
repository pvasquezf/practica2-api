﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apis_Practica2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Apis_Practica2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        // GET: api/Usuario
        [HttpGet]
        public String Get()
        {
            try
            {
                string query = "select * from usuario;";
                Conexion conn = new Conexion();
                List<Generico2> lst1 = conn.metodo_consulta(query, 4);
                List<Usuario> lstUsuarios = new List<Usuario>();
                foreach (Generico2 item in lst1)
                {
                    Usuario new_user = new Usuario()
                    {
                        Id_usuario = Convert.ToInt32(item.Lst[0].Parametro.ToString()),
                        User_name = item.Lst[1].Parametro.ToString(),
                        Correo = item.Lst[2].Parametro.ToString(),
                        Pass = item.Lst[3].Parametro.ToString()
                    };
                    lstUsuarios.Add(new_user);
                }
                return JsonConvert.SerializeObject(lstUsuarios);
            }
            catch (Exception)
            {

                return "NO EXISTEN DATOS";
            }
        }


        // POST: api/Usuario
        [HttpPost]
        public bool Post(Usuario entrada)
        {
            try
            {
                Conexion conn = new Conexion();
                List<Generico> lst = new List<Generico>
                {
                    new Generico("user_name",entrada.User_name,2),
                    new Generico("correo",entrada.Correo,2),
                    new Generico("pass",entrada.Pass,2)
                };
                return conn.metodo_proc("insertUsuario", lst);
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
