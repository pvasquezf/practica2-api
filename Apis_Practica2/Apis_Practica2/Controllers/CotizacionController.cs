﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apis_Practica2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Apis_Practica2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CotizacionController : ControllerBase
    {
        // GET: api/Cotizacion/5
        [HttpGet("{id}", Name = "Get2")]
        public string Get(int id)
        {
            try
            {
                string query = "Select * from cotizacion where id_cuenta=" + id;
                Conexion conn = new Conexion();
                List<Generico2> lst1 = conn.metodo_consulta(query, 4);
                List<Cotizacion> cotizacions = new List<Cotizacion>();
                for (int i = 0; i < lst1.Count; i++)
                {
                    Cotizacion nuevo = new Cotizacion(
                        Convert.ToInt32(lst1[i].Lst[0].Parametro.ToString()),
                        Convert.ToInt32(lst1[i].Lst[1].Parametro.ToString()),
                        lst1[i].Lst[2].Parametro.ToString(),
                        Convert.ToDouble(lst1[i].Lst[3].Parametro.ToString())
                        );
                    cotizacions.Add(nuevo);
                }
                return JsonConvert.SerializeObject(cotizacions);
            }
            catch { return "NO EXISTEN DATOS"; }
        }

    

    // POST: api/Cotizacion
    [HttpPost]
        public int Post(Cotizacion entrada)
        {
            try
            {
                Conexion conn = new Conexion();
                List<Generico> lst = new List<Generico>
                {
                    new Generico("id_cuenta", entrada.Id_cuenta, 1)
                };

                return conn.Post("insertCotizacion", lst);
            }
            catch
            {
                return -1;

            }

        }

        // PUT: api/Cotizacion/5
        [HttpPut("{id}")]
        public bool Put(int id, Cotizacion entrada)
        {
            entrada.Id_cotizacion = id;
            try
            {
                Conexion conn = new Conexion();
                List<Generico> lst = new List<Generico>
                {
                    new Generico("id_cotizacion",entrada.Id_cotizacion,1),
                    new Generico("total", entrada.Precio_total, 4),
                };

                return conn.metodo_proc("updateCotizacion", lst);
            }
            catch
            {
                return false;

            }
        }
    }
}
