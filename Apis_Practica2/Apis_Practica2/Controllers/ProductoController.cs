﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Apis_Practica2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Apis_Practica2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        // GET: api/Producto
        [HttpGet]
        public string Get()
        {
            try
            {
                string query = "Select * from producto;";
                Conexion conn = new Conexion();
                List<Generico2> lst1 = conn.metodo_consulta(query, 4);
                List<Producto> productos = new List<Producto>();
                for (int i = 0; i < lst1.Count; i++)
                {
                    Producto nuevo = new Producto(
                        Convert.ToInt32(lst1[i].Lst[0].Parametro.ToString()),
                        lst1[i].Lst[1].Parametro.ToString(),
                        lst1[i].Lst[2].Parametro.ToString(),
                        Convert.ToDouble(lst1[i].Lst[3].Parametro.ToString())
                        );
                    productos.Add(nuevo);
                }

                return JsonConvert.SerializeObject(productos);
            }
            catch { return "NO EXISTEN DATOS"; }
        }

    }
}
