﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Apis_Practica2.Models
{
    public class Cotizacion
    {
        int id_cotizacion, id_cuenta;
        string fecha_cotizacion;
        double precio_total;

        public Cotizacion(int id_cotizacion, int id_cuenta, string fecha_cotizacion, double precio_total)
        {
            this.id_cotizacion = id_cotizacion;
            this.id_cuenta = id_cuenta;
            this.fecha_cotizacion = fecha_cotizacion;
            this.precio_total = precio_total;
        }

        public int Id_cotizacion { get => id_cotizacion; set => id_cotizacion = value; }
        public int Id_cuenta { get => id_cuenta; set => id_cuenta = value; }
        public string Fecha_cotizacion { get => fecha_cotizacion; set => fecha_cotizacion = value; }
        public double Precio_total { get => precio_total; set => precio_total = value; }
    }
}
